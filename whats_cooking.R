require("rjson")
require("stringi")
require("dplyr")
require("ggplot2")
library(e1071)
library(caret)

dados_json <- fromJSON(file = "train.json")

dados = lapply(dados_json, tratarDadosReceita)
dados  <-  as.data.frame(do.call(rbind, dados))
names(dados) <- c('x_data', 'y_data')

dados_fragmentados <- data.frame(id_recipe=integer(), country=character(), ingredient=character())
for(r in dados_json){
  for(ingredient in r[3]){
    dados_fragmentados <- rbind(dados_fragmentados, 
                                data.frame(id_recipe=r[[1]], country=r[[2]], ingredient=ingredient))
  }
}


indices <- createDataPartition(unlist(dados$country), p=0.80, list=FALSE)

treino <- dados[indices,]
teste <- dados[-indices,]

rf <- train(country~., data=treino, method="rf")
predicoes.rf <- predict(rf, teste)

svm <- train(country~., data=treino, method="svmRadial")
predicoes.svm <- predict(svm, teste)

rna <- train(country~., data=treino, method="nnet", trace=FALSE)
predicoes.rna = predict(rna, teste)


occurences_ingredients_in_country <- dados_fragmentados %>%
  group_by(country, ingredient) %>%
  tally()

recipes_number_of_ingredients <- dados_fragmentados %>%
  group_by(country, id_recipe) %>%
  tally()

occurences_ingredients_countries <- dados_fragmentados %>%
  group_by(country) %>%
  group_by(ingredient) %>%
  tally




tratarDadosReceita <- function (receita){
  id = receita[[1]]
  country = receita[[2]]
  names(country) <- NULL
  ingredients = receita[[3]]
  
  ingredients <- sapply(ingredients, function(x){
    return(gsub(" ", "", x))
  })
  recipe = NULL
  for (w in ingredients){
    recipe <- paste(recipe,w)
  }

  return(data.frame(id = id, country = country, ingredients=recipe))
}

unique_countries <- unlist(unique(dados$x_data))

words_occurences_in_country <- lapply(unique_countries, function(c){
    recipes_in_country <- dados[dados$x_data==c,]

})


