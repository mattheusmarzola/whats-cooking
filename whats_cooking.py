#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jul 19 13:36:56 2019

@author: mrml
"""
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction.text import TfidfTransformer
from sklearn.svm import SVC
import json
from sklearn.pipeline import Pipeline
from sklearn.naive_bayes import MultinomialNB
from sklearn.svm import LinearSVC
import matplotlib.pyplot as plt
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import confusion_matrix
import numpy as np
from sklearn import metrics
from sklearn.model_selection import train_test_split
import random, re
from sklearn.model_selection import cross_val_predict



def read_dataset(path):
	return json.load(open(path)) 


def generate_text(data, op):
    if(op == 2):
        data = [ " ".join(doc).lower() 
                    for doc in [[i.replace(' ','') 
                    for i in doc['ingredients']] 
                    for doc in train]]
        return [x for x in data]
    elif op == 1 :
        data = [" ".join(doc['ingredients']).lower() for doc in data]
        return [x for x in data]
    elif op == 3 :
        data = [ " ".join(doc).lower() 
                    for doc in [[i.replace('japonese','')
                                  .replace('japonese','') 
                                    for i in doc['ingredients']] 
                                    for doc in train]]
        return [x for x in data]
    
def count_recipes(train):
    data = [[doc['id'], len(doc['ingredients'])] for doc in train]
    return data

def primeira_rodada(train, op_word):

    train_text = generate_text(train, op_word)
    
    target = [doc['cuisine'] for doc in train]
    
    x_train, x_test, y_train, y_test = train_test_split(
           train_text, target, test_size=0.25, random_state=None)

    print ("Multinomial")    
    execute_model(x_train, y_train, x_test, y_test, 
                      MultinomialNB(),
                      'Multinomial')
    print ("Random Forest")
    execute_model(x_train, y_train, x_test, y_test, 
                      RandomForestClassifier(), 
                      'Random Forest')
    print ("Linear_SVC")
    execute_model(x_train, y_train, x_test, y_test, 
                      LinearSVC(), 
                      'Linear_SVC')
    print ("SVC")
    execute_model(x_train, y_train, x_test, y_test, 
                      SVC(), 
                      'SVC')

def segunda_rodada(train, op_word):
    train_text = generate_text(train, op_word)
    
    target = [doc['cuisine'] for doc in train]
    
    x_train, x_test, y_train, y_test = train_test_split(
           train_text, target, test_size=0.25, random_state=None)


    word_counts_countries = count_words(x_train, y_train)
    word_total_occurrences = defaultdict( lambda: [0,0], 
                         [ [x[0] , sum(list(x[1].values()))] 
                         for x in word_counts_countries.items() ]) 
    #REMOVENDO AS PALAVRAS QUE APARECEM MAIS QUE 10k
    label_list = [x[0] for x in list(sorted(word_total_occurrences.items(),
                                              key=lambda v: v[1]))]
    number_list = [x[1] for x in list(sorted(word_total_occurrences.items(), 
                                               key=lambda v: v[1]))]
    label_list = label_list[-4:]
    number_list = number_list[-4:]
    
    for w in label_list:
        train_text = [x.replace(w+' ', '') for x in train_text]
        train_text = [x.replace(' '+w, '') for x in train_text]
    
    
    x_train, x_test, y_train, y_test = train_test_split(
           train_text, target, test_size=0.25, random_state=None)
    
    print ("Multinomial")    
    execute_model(x_train, y_train, x_test, y_test, 
                      MultinomialNB(),
                      'Multinomial')
    print ("Random Forest")
    execute_model(x_train, y_train, x_test, y_test, 
                      RandomForestClassifier(), 
                      'Random Forest')
    print ("Linear_SVC")
    execute_model(x_train, y_train, x_test, y_test, 
                      LinearSVC(), 
                      'Linear_SVC')
    print ("SVC")
    execute_model(x_train, y_train, x_test, y_test, 
                      SVC(), 
                      'SVC')
    

def terceira_rodada(train, op_word):
    
    train_text = generate_text(train, op_word)
    
    target = [doc['cuisine'] for doc in train]
    
    x_train, x_test, y_train, y_test = train_test_split(
           train_text, target, test_size=0.25, random_state=None)


    word_counts_countries = count_words(x_train, y_train)
    word_total_occurrences = defaultdict( lambda: [0,0], 
                         [ [x[0] , sum(list(x[1].values()))] 
                         for x in word_counts_countries.items() ]) 
    #REMOVENDO AS PALAVRAS QUE APARECEM MAIS QUE 10k
    label_list = [x[0] for x in list(sorted(word_total_occurrences.items(),
                                              key=lambda v: v[1]))]
    number_list = [x[1] for x in list(sorted(word_total_occurrences.items(), 
                                               key=lambda v: v[1]))]
    label_list = label_list[-4:]
    number_list = number_list[-4:]
    
    for w in label_list:
        train_text = [x.replace(w+' ', '') for x in train_text]
        train_text = [x.replace(' '+w, '') for x in train_text]
    
    
    x_train, x_test, y_train, y_test = train_test_split(
           train_text, target, test_size=0.25, random_state=None)
    
    print ("Multinomial") 
    execute_model(x_train, y_train, x_test, y_test, 
                  MultinomialNB(alpha=0.1,
                                fit_prior=True
                                ),
                  'Multinomial')
    print ("Random Forest") 
    execute_model(x_train, y_train, x_test, y_test, 
                  RandomForestClassifier(bootstrap=False,  
                                         min_samples_leaf=1,
                                         min_samples_split=2, 
                                         n_estimators=15, 
                                         n_jobs=1, 
                                         oob_score=False, 
                                         random_state=None,
                                         verbose=0, 
                                         warm_start=False), 
                  'Random Forest')
    print("Linear_SVC")          
    execute_model(x_train, y_train, x_test, y_test, 
                      LinearSVC(penalty = 'l2',
                                loss='squared_hinge',
                                dual=False,
                                tol=1e-2,
                                C=1,
                                multi_class='crammer_singer',
                                max_iter=-1), 
                      'Linear_SVC')
    print ("SVC") 
    execute_model(x_train, y_train, x_test, y_test, SVC(C=100, 
    	 			 kernel='rbf', 
    	 			 degree=3, 
    	 			 gamma=1, 
    	 			 coef0=1, 
    	 			 shrinking=True, 
    	 			 tol=0.001, 
    	      		 probability=False, 
    	      		 cache_size=200, 
    	      		 class_weight=None, 
    	      		 verbose=False, 
    	      		 max_iter=-1, 
              		 decision_function_shape=None, 
              		 random_state=None), 'SVC')

def quarta_rodada(train, op_word):

    train_text = generate_text(train, op_word)
    
    target = [doc['cuisine'] for doc in train]
    
    x_train, x_test, y_train, y_test = train_test_split(
           train_text, target, test_size=0.25, random_state=None)

    print ("Multinomial") 
    execute_model(x_train, y_train, x_test, y_test, 
                  MultinomialNB(alpha=0.1,
                                fit_prior=True
                                ),
                  'Multinomial')
                  
    print ("Random Forest") 
    execute_model(x_train, y_train, x_test, y_test, 
                  RandomForestClassifier(bootstrap=False,  
                                         min_samples_leaf=1,
                                         min_samples_split=2, 
                                         n_estimators=15, 
                                         n_jobs=1, 
                                         oob_score=False, 
                                         random_state=None,
                                         verbose=0, 
                                         warm_start=False), 
                  'Random Forest')
    print("Linear_SVC")          
    execute_model(x_train, y_train, x_test, y_test, 
                      LinearSVC(penalty = 'l2',
                                loss='squared_hinge',
                                dual=False,
                                tol=1e-2,
                                C=1,
                                multi_class='crammer_singer',
                                max_iter=-1), 
                      'Linear_SVC')
    print ("SVC") 
    execute_model(x_train, y_train, x_test, y_test, SVC(C=100, 
    	 			 kernel='rbf', 
    	 			 degree=3, 
    	 			 gamma=1, 
    	 			 coef0=1, 
    	 			 shrinking=True, 
    	 			 tol=0.001, 
    	      		 probability=False, 
    	      		 cache_size=200, 
    	      		 class_weight=None, 
    	      		 verbose=False, 
    	      		 max_iter=-1, 
              		 decision_function_shape=None, 
              		 random_state=None), 'SVC')


def quinta_rodada(train, op_word):

    train_text = generate_text(train, op_word)
    
    target = [doc['cuisine'] for doc in train]


    print ("Multinomial") 
    execute_model_cv(train_text, target, 
                  MultinomialNB(alpha=0.1,
                                fit_prior=True
                                ),
                  'Multinomial')
                  
    print ("Random Forest") 
    execute_model_cv(train_text, target, 
                  RandomForestClassifier(bootstrap=False,  
                                         min_samples_leaf=1,
                                         min_samples_split=2, 
                                         n_estimators=15, 
                                         n_jobs=1, 
                                         oob_score=False, 
                                         random_state=None,
                                         verbose=0, 
                                         warm_start=False), 
                  'Random Forest')
    print("Linear_SVC")          
    execute_model_cv(train_text, target,  
                      LinearSVC(penalty = 'l2',
                                loss='squared_hinge',
                                dual=False,
                                tol=1e-2,
                                C=1,
                                multi_class='crammer_singer',
                                max_iter=-1), 
                      'Linear_SVC')
    print ("SVC") 
    execute_model_cv(train_text, target, 
                  SVC(C=100, 
    	 			 kernel='rbf', 
    	 			 degree=3, 
    	 			 gamma=1, 
    	 			 coef0=1, 
    	 			 shrinking=True, 
    	 			 tol=0.001, 
    	      		 probability=False, 
    	      		 cache_size=200, 
    	      		 class_weight=None, 
    	      		 verbose=False, 
    	      		 max_iter=-1, 
              		 decision_function_shape=None, 
              		 random_state=None), 'SVC')
    
    
def sexta_rodada(train, op_word):

    filtered_recipes = remover_receitas_outline(train)
    train =  [recipe for recipe in train
              if recipe['id'] in filtered_recipes['id'] ]
    
    train_text = generate_text(train, op_word)
    target = [doc['cuisine'] for doc in train]

    train_text = remove_targe_from_data(train_text, target)
    
    word_counts_countries = count_words(x_train, y_train)
    word_total_occurrences = defaultdict( lambda: [0,0], 
                         [ [x[0] , sum(list(x[1].values()))] 
                         for x in word_counts_countries.items() ]) 
    #REMOVENDO AS PALAVRAS QUE APARECEM MAIS QUE 10k
    label_list = [x[0] for x in list(sorted(word_total_occurrences.items(),
                                              key=lambda v: v[1]))]
    number_list = [x[1] for x in list(sorted(word_total_occurrences.items(), 
                                               key=lambda v: v[1]))]
    label_list = label_list[-4:]
    number_list = number_list[-4:]
    
    for w in label_list:
        train_text = [x.replace(w+' ', '') for x in train_text]
        train_text = [x.replace(' '+w, '') for x in train_text]
        
    label_list = label_list[100:]
    number_list = number_list[100:]
    
    for w in label_list:
        train_text = [x.replace(w+' ', '') for x in train_text]
        train_text = [x.replace(' '+w, '') for x in train_text]
        
    train_text = [x.replace('(', '') for x in train_text]
    train_text = [x.replace(')', '') for x in train_text]
    train_text = [x.replace('1', '') for x in train_text]
    train_text = [x.replace('2', '') for x in train_text]
    train_text = [x.replace('3', '') for x in train_text]
    train_text = [x.replace('4', '') for x in train_text]
    train_text = [x.replace('5', '') for x in train_text]
    train_text = [x.replace('7', '') for x in train_text]
    train_text = [x.replace('8', '') for x in train_text]
    train_text = [x.replace('9', '') for x in train_text]
    train_text = [x.replace('0', '') for x in train_text]
    train_text = [x.replace('10', '') for x in train_text]
    train_text = [x.replace('oz.', '') for x in train_text]
    train_text = [x.replace('%', '') for x in train_text]
    
    print ("Multinomial") 
    execute_model_cv(train_text, target, 
                  MultinomialNB(alpha=0.1,
                                fit_prior=True
                                ),
                  'Multinomial')
                  
    print ("Random Forest") 
    execute_model_cv(train_text, target, 
                  RandomForestClassifier(bootstrap=False,  
                                         min_samples_leaf=1,
                                         min_samples_split=2, 
                                         n_estimators=15, 
                                         n_jobs=1, 
                                         oob_score=False, 
                                         random_state=None,
                                         verbose=0, 
                                         warm_start=False), 
                  'Random Forest')
    print("Linear_SVC")          
    execute_model_cv(train_text, target,  
                      LinearSVC(penalty = 'l2',
                                loss='squared_hinge',
                                dual=False,
                                tol=1e-2,
                                C=1,
                                multi_class='crammer_singer',
                                max_iter=-1), 
                      'Linear_SVC')
    print ("SVC") 
    execute_model_cv(train_text, target, 
                  SVC(C=100, 
    	 			 kernel='rbf', 
    	 			 degree=3, 
    	 			 gamma=1, 
    	 			 coef0=1, 
    	 			 shrinking=True, 
    	 			 tol=0.001, 
    	      		 probability=False, 
    	      		 cache_size=200, 
    	      		 class_weight=None, 
    	      		 verbose=False, 
    	      		 max_iter=-1, 
              		 decision_function_shape=None, 
              		 random_state=None), 'SVC')
    
def remover_receitas_outline(train):
    counted_recipes = count_recipes(train)
    df = pd.DataFrame(counted_recipes)
    df.columns = ['id', 'ingredients']
    df = df [df['ingredients'] < 25]
    return df;

def remove_targe_from_data(train_text, target):
    df_y = pd.DataFrame(target)
    df_y.columns = ['country']
    y_set = set(df_y['country'])
    
     
    data = [' '.join(w for w in recipe.split() if w.lower() not in y_set)
                     for recipe in train_text
                     ]
    return data;



#MELHORIAS EXECUTADAS
#train_text = [x.replace('salt ', '') for x in train_text]
#train_text = [x.replace(' salt', '') for x in train_text]
#train_text = [x.replace('tomatoes ', '') for x in train_text]
#train_text = [x.replace(' tomatoes', '') for x in train_text]
#train_text = [x.replace(' tomato', '') for x in train_text]
#train_text = [x.replace('tomato ', '') for x in train_text]



#realizando a contagem de palavras
#word_counts_countries = count_words(x_train, y_train)
#
#
#word_countries = defaultdict( lambda: [0,0], 
#                             [ [x[0] , len(x[1])] 
#                             for x in word_counts_countries.items() ]) 
#    
#    
#word_total_occurrences = defaultdict( lambda: [0,0], 
#                         [ [x[0] , sum(list(x[1].values()))] 
#                         for x in word_counts_countries.items() ]) 
#    
#count_occurrences = Counter(word_total_occurrences.values())
#index = np.arange(len(numero_de_paises))
#plt.bar(index, numero_de_paises)
#plt.xlabel('Genre', fontsize=5)
#plt.ylabel('No of Movies', fontsize=5)
#plt.xticks(index, ingredientes_que_aparecem_em_um_pais, fontsize=5, rotation=30)
#plt.title('Word Total Occurrences')
#plt.show()   
#    
#
#
#
##REMOVENDO AS PALAVRAS QUE APARECEM EM APENAS 1 PAIS
#label_list = [x[0] for x in list(sorted(word_countries.items(), key=lambda v: v[1]))]
#number_list = [x[1] for x in list(sorted(word_countries.items(), key=lambda v: v[1]))]
#
#numero_de_paises = list(filter(lambda x : x ==1, number_list))
#ingredientes_que_aparecem_em_um_pais = label_list[:len(numero_de_paises)]
#
#for w in ingredientes_que_aparecem_em_um_pais:
#    train_text = [x.replace(w+' ', '') for x in train_text]
#    train_text = [x.replace(' '+w, '') for x in train_text]
#
#
#
#
##REMOVENDO AS PALAVRAS QUE APARECEM MAIS QUE 10k
#label_list = [x[0] for x in list(sorted(word_total_occurrences.items(),
#                                          key=lambda v: v[1]))]
#number_list = [x[1] for x in list(sorted(word_total_occurrences.items(), 
#                                           key=lambda v: v[1]))]
#label_list = label_list[-4:]
#number_list = number_list[-4:]
#
#for w in label_list:
#    train_text = [x.replace(w+' ', '') for x in train_text]
#    train_text = [x.replace(' '+w, '') for x in train_text]
#
#
#x_train, x_test, y_train, y_test = train_test_split(
#       train_text, target, test_size=0.25, random_state=None)




def execute_model(x_train, y_train, x_test, y_test, algorithm, label):
    text_clf = Pipeline([('vect', CountVectorizer()),
                         ('tfidf', TfidfTransformer()),
                         ('clf', algorithm),
    ])
    
    text_clf.fit(x_train, y_train)  
    predicted = text_clf.predict(x_test)
    print ("Matriz de confusão "+label) 
    print(metrics.classification_report(y_test, predicted))
    
    metrics.confusion_matrix(y_test, predicted)
    
    plot_confusion_matrix(y_test, predicted, classes=set(y_test),
                          title='Confusion Matrix '+label) 
   
def execute_model_cv(x_data, y_data, algorithm, label):
    text_clf = Pipeline([('vect', CountVectorizer()),
                         ('tfidf', TfidfTransformer()),
                         ('clf', algorithm),
    ])
    
    print ("cross_val_predict "+label)
    
    predicted = cross_val_predict(text_clf, x_data, y_data, cv=10)
    
    print(metrics.accuracy_score(y_data, predicted))
    
    print ("Matriz de confusão "+label) 
    print(metrics.classification_report(y_data, predicted))
    
    metrics.confusion_matrix(y_data, predicted)
    
    plot_confusion_matrix(y_data, predicted, classes=set(y_data),
                          title='Confusion Matrix '+label) 
        
    
def count_words(x_train, y_train):
    counts = defaultdict(lambda: defaultdict( lambda: 0 ))
    for ingredients, country in zip(x_train,y_train):
        tokens = tokenize(ingredients)
        for word in tokens:
            counts[word][country] += 1
    
    return counts

def tokenize(ingredients):
      
    ingredients = ingredients.lower()                   
    
    if( len(re.findall("[0-9']+", ingredients)) > 0 ):
        ingredients+=" __contains_numbers__ "
        number_regex = re.compile("[0-9']+")    
        ingredients = number_regex.sub("", ingredients)        
    
    all_words = re.findall("[a-z0-9']+", ingredients) 
    word_set = set()
    
    for word in all_words :
        if(not word.isdigit()):
            word_set.add( word )    
    
    return word_set  


def plot_confusion_matrix(y_true, y_pred, classes,
                          normalize=False,
                          title=None,
                          cmap=plt.cm.Blues):
    if not title:
        if normalize:
            title = 'Normalized confusion matrix'
        else:
            title = 'Confusion matrix, without normalization'
            
    cm = confusion_matrix(y_true, y_pred)
    if normalize:
        cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]
        print("Normalized confusion matrix")
    else:
        print('Confusion matrix, without normalization')


    fig, ax = plt.subplots(figsize = [10.0, 10.0])
    
    im = ax.imshow(cm, interpolation='nearest', cmap=cmap)
    ax.figure.colorbar(im, ax=ax)
    ax.set(xticks=np.arange(cm.shape[1]),
           yticks=np.arange(cm.shape[0]),
           xticklabels=classes, yticklabels=classes,
           title=title,
           ylabel='True label',
           xlabel='Predicted label',
           )

    plt.setp(ax.get_xticklabels(), rotation=45, ha="right",
             rotation_mode="anchor")
    
    fmt = '.2f' if normalize else 'd'
    thresh = cm.max() / 2.
    for i in range(cm.shape[0]):
        for j in range(cm.shape[1]):
            ax.text(j, i, format(cm[i, j], fmt),
                    ha="center", va="center",
                    color="white" if cm[i, j] > thresh else "black")
    fig.tight_layout()
    return ax


   
if __name__ == "__main__":
    random.seed(53646)

    train = read_dataset(
        '/home/mrml/Documentos/material_iaa/whats_cooking/train.json')

    primeira_rodada(train, 1)

    segunda_rodada(train, 1)
    
    terceira_rodada(train, 1)
    
    quarta_rodada(train, 1)
    
    quinta_rodada(train, 1)
    
    sexta_rodada(train, 1)




